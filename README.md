In software development there are developers, product managers, scrum teams , implementers and support. Agile methods increasingly increase the distance of the end user, who are the purpose why we build software. For example, while using EMR, implementers may be technical people too far removed from the doctors, nurses and hospital administrators 
who use the system. In my own quest to collect user need, paper and pen are the best tools. But when there is an opportunity to collect user needs across continents for open source 
products that I use, then this is a difficult task .. with too many papers 


I view the user stories as a promise to have communication. And we need to track these conversations. In fact, I would like to build software truly needed by the end users - with every technical component requested by an end user to meet a specific need (and make the world a better place) ……

Please refer to the Wiki to see sample functionality 

https://gitlab.com/judywawira/wateja/wikis/home

PS: Wateja - is Swahili for end user needs 


